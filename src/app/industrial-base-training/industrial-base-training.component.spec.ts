import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustrialBaseTrainingComponent } from './industrial-base-training.component';

describe('IndustrialBaseTrainingComponent', () => {
  let component: IndustrialBaseTrainingComponent;
  let fixture: ComponentFixture<IndustrialBaseTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustrialBaseTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustrialBaseTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
