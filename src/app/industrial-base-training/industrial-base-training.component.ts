import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-industrial-base-training',
  templateUrl: './industrial-base-training.component.html',
  styleUrls: ['./industrial-base-training.component.sass']
})
export class IndustrialBaseTrainingComponent implements OnInit {
  quotes: any = [];
  randomQuote: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/quotes.json").subscribe(data => {
      this.quotes = data;
      this.randomQuote = this.quotes[Math.floor(Math.random() * this.quotes.length)];
    })

    console.clear();
  }

}
