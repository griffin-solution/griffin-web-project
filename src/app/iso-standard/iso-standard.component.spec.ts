import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IsoStandardComponent } from './iso-standard.component';

describe('IsoStandardComponent', () => {
  let component: IsoStandardComponent;
  let fixture: ComponentFixture<IsoStandardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsoStandardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsoStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
