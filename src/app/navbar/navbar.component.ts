import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
  name = 'Get Current Url Route Demo';
  currentRoute: any = '';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {

   }

  ngOnInit(): void {
    console.log(this.router.url);
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event) => {
      this.currentRoute = event;
      // console.log(this.currentRoute);
    });

    $(".features").on("click", function(){
        $('.dropdown-menu').dropdown('toggle')
      })

    $(window).scroll(() => {
      if ($(window).scrollTop() > 53) {
        $('nav').addClass('shrink');
      }
      if ($(window).scrollTop() < 54) {
        $('nav').removeClass('shrink');
      }
    });

    $('#showRight').click(function() {
      $('.menu-right').toggleClass('right-open');
    });

    $('.backBtn').click(function() {
      $('.menu').removeClass('top-open bottom-open right-open left-open pushleft-open pushright-open');
      $('body').removeClass('push-toleft push-toright');
    });

    //mobile slider menu
    $('[data-toggle="offcanvas"], #navToggle').on('click', function () {
        $('.offcanvas-collapse').toggleClass('open')
    });

    $('#nav-hamburger').click(function(){
        $('#nav-hamburger').toggleClass('open');
    });

    $('.cil-x').click(() => {
        $('.offcanvas-collapse').removeClass('open');
        $('#nav-hamburger').removeClass('open');
    });

    $('.navbar-collapse a').click(function(){
      $(".navbar-collapse").collapse('hide');
    });
  }

}
