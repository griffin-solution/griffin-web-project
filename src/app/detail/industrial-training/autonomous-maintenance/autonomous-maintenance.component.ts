import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-autonomous-maintenance',
  templateUrl: './autonomous-maintenance.component.html',
  styleUrls: ['./autonomous-maintenance.component.sass']
})
export class AutonomousMaintenanceComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/autonomous-maintenance.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/autonomous-maintenance-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
