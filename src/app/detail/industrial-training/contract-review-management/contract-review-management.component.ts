import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contract-review-management',
  templateUrl: './contract-review-management.component.html',
  styleUrls: ['./contract-review-management.component.sass']
})
export class ContractReviewManagementComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/contract-review-management.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/contract-review-management-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
