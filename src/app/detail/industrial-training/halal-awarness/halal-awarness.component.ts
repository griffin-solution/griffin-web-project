import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-halal-awarness',
  templateUrl: './halal-awarness.component.html',
  styleUrls: ['./halal-awarness.component.sass']
})
export class HalalAwarnessComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/halal-awarness.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/halal-awarness-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
