import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalalAwarnessComponent } from './halal-awarness.component';

describe('HalalAwarnessComponent', () => {
  let component: HalalAwarnessComponent;
  let fixture: ComponentFixture<HalalAwarnessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalalAwarnessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalalAwarnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
