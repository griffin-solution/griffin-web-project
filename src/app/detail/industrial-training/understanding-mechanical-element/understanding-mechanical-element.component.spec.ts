import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderstandingMechanicalElementComponent } from './understanding-mechanical-element.component';

describe('UnderstandingMechanicalElementComponent', () => {
  let component: UnderstandingMechanicalElementComponent;
  let fixture: ComponentFixture<UnderstandingMechanicalElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnderstandingMechanicalElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderstandingMechanicalElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
