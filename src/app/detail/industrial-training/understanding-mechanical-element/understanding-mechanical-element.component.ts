import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-understanding-mechanical-element',
  templateUrl: './understanding-mechanical-element.component.html',
  styleUrls: ['./understanding-mechanical-element.component.sass']
})
export class UnderstandingMechanicalElementComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/understanding-mechanical-element.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/understanding-mechanical-element-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
