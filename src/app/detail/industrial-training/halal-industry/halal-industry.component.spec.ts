import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalalIndustryComponent } from './halal-industry.component';

describe('HalalIndustryComponent', () => {
  let component: HalalIndustryComponent;
  let fixture: ComponentFixture<HalalIndustryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalalIndustryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalalIndustryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
