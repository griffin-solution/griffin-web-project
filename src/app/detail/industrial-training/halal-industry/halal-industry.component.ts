import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-halal-industry',
  templateUrl: './halal-industry.component.html',
  styleUrls: ['./halal-industry.component.sass']
})
export class HalalIndustryComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/best-practice-halal-industry.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/best-practice-halal-industry-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
