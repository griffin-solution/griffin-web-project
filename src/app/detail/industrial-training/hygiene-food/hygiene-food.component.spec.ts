import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HygieneFoodComponent } from './hygiene-food.component';

describe('HygieneFoodComponent', () => {
  let component: HygieneFoodComponent;
  let fixture: ComponentFixture<HygieneFoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HygieneFoodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HygieneFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
