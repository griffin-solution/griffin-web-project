import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hidraulics',
  templateUrl: './hidraulics.component.html',
  styleUrls: ['./hidraulics.component.sass']
})
export class HidraulicsComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/industrial-pneumatic.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/industrial-pneumatic-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
