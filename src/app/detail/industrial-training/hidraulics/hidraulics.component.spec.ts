import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HidraulicsComponent } from './hidraulics.component';

describe('HidraulicsComponent', () => {
  let component: HidraulicsComponent;
  let fixture: ComponentFixture<HidraulicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HidraulicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HidraulicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
