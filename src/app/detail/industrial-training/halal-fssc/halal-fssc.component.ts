import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-halal-fssc',
  templateUrl: './halal-fssc.component.html',
  styleUrls: ['./halal-fssc.component.sass']
})
export class HalalFsscComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/halal-fssc.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/halal-fssc-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
