import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalalFsscComponent } from './halal-fssc.component';

describe('HalalFsscComponent', () => {
  let component: HalalFsscComponent;
  let fixture: ComponentFixture<HalalFsscComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalalFsscComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalalFsscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
