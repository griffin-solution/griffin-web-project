import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAllianceComponent } from './business-alliance.component';

describe('BusinessAllianceComponent', () => {
  let component: BusinessAllianceComponent;
  let fixture: ComponentFixture<BusinessAllianceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessAllianceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAllianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
