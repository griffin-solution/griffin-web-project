import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-business-alliance',
  templateUrl: './business-alliance.component.html',
  styleUrls: ['./business-alliance.component.sass']
})
export class BusinessAllianceComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/industrial-training-detail/responsible-business-alliance.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/industrial-training-detail/responsible-business-alliance-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
