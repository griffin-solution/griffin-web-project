import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-effective-business-english-writing',
  templateUrl: './effective-business-english-writing.component.html',
  styleUrls: ['./effective-business-english-writing.component.sass']
})
export class EffectiveBusinessEnglishWritingComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/effective-bussiness-english.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/effective-bussiness-english-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
