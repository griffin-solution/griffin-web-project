import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectiveBusinessEnglishWritingComponent } from './effective-business-english-writing.component';

describe('EffectiveBusinessEnglishWritingComponent', () => {
  let component: EffectiveBusinessEnglishWritingComponent;
  let fixture: ComponentFixture<EffectiveBusinessEnglishWritingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EffectiveBusinessEnglishWritingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectiveBusinessEnglishWritingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
