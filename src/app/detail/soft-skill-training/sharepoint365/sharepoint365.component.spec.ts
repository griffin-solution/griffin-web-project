import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Sharepoint365Component } from './sharepoint365.component';

describe('Sharepoint365Component', () => {
  let component: Sharepoint365Component;
  let fixture: ComponentFixture<Sharepoint365Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Sharepoint365Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Sharepoint365Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
