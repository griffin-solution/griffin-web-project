import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sharepoint365',
  templateUrl: './sharepoint365.component.html',
  styleUrls: ['./sharepoint365.component.sass']
})
export class Sharepoint365Component implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/sharepoint-365-end-user-training.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/sharepoint-365-end-user-training-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }
}
