import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagingEmotionalIntelligenceComponent } from './managing-emotional-intelligence.component';

describe('ManagingEmotionalIntelligenceComponent', () => {
  let component: ManagingEmotionalIntelligenceComponent;
  let fixture: ComponentFixture<ManagingEmotionalIntelligenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagingEmotionalIntelligenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagingEmotionalIntelligenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
