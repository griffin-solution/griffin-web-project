import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-managing-emotional-intelligence',
  templateUrl: './managing-emotional-intelligence.component.html',
  styleUrls: ['./managing-emotional-intelligence.component.sass']
})
export class ManagingEmotionalIntelligenceComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/managing-emotional-intelligence.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/managing-emotional-intelligence-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
