import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrademarkProgramComponent } from './trademark-program.component';

describe('TrademarkProgramComponent', () => {
  let component: TrademarkProgramComponent;
  let fixture: ComponentFixture<TrademarkProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrademarkProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrademarkProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
