import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeakingForSuccessComponent } from './speaking-for-success.component';

describe('SpeakingForSuccessComponent', () => {
  let component: SpeakingForSuccessComponent;
  let fixture: ComponentFixture<SpeakingForSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpeakingForSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeakingForSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
