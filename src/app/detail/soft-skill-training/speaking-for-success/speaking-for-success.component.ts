import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-speaking-for-success',
  templateUrl: './speaking-for-success.component.html',
  styleUrls: ['./speaking-for-success.component.sass']
})
export class SpeakingForSuccessComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/speaking-for-success.json").subscribe(data => {
      this.contents = data;
    })

    console.clear();
  }

}
