import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-story-telling',
  templateUrl: './story-telling.component.html',
  styleUrls: ['./story-telling.component.sass']
})
export class StoryTellingComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/story-telling-for-influence.json").subscribe(data => {
      this.contents = data;
    })

    console.clear();
  }
}
