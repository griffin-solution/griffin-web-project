import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnleashYourPotentialComponent } from './unleash-your-potential.component';

describe('UnleashYourPotentialComponent', () => {
  let component: UnleashYourPotentialComponent;
  let fixture: ComponentFixture<UnleashYourPotentialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnleashYourPotentialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnleashYourPotentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
