import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unleash-your-potential',
  templateUrl: './unleash-your-potential.component.html',
  styleUrls: ['./unleash-your-potential.component.sass']
})
export class UnleashYourPotentialComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/unleash-your-potential.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/unleash-your-potential-content.json").subscribe(data => {
      this.training = data;
    })
    console.clear()
  }
}
