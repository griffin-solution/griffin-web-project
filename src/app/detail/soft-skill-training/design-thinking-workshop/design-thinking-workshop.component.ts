import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-design-thinking-workshop',
  templateUrl: './design-thinking-workshop.component.html',
  styleUrls: ['./design-thinking-workshop.component.sass']
})
export class DesignThinkingWorkshopComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/design-thinking-workshop.json").subscribe(data => {
      this.contents = data;
    })

    console.clear();
  }

}
