import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignThinkingWorkshopComponent } from './design-thinking-workshop.component';

describe('DesignThinkingWorkshopComponent', () => {
  let component: DesignThinkingWorkshopComponent;
  let fixture: ComponentFixture<DesignThinkingWorkshopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignThinkingWorkshopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignThinkingWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
