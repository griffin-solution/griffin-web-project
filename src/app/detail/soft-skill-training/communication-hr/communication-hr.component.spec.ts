import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationHrComponent } from './communication-hr.component';

describe('CommunicationHrComponent', () => {
  let component: CommunicationHrComponent;
  let fixture: ComponentFixture<CommunicationHrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunicationHrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationHrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
