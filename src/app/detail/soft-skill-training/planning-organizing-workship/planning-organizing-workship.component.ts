import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planning-organizing-workship',
  templateUrl: './planning-organizing-workship.component.html',
  styleUrls: ['./planning-organizing-workship.component.sass']
})
export class PlanningOrganizingWorkshipComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/planning-and-organizing-workshop.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/planning-and-organizing-workshop-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
