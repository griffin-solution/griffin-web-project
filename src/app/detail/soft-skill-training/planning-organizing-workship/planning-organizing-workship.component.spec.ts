import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningOrganizingWorkshipComponent } from './planning-organizing-workship.component';

describe('PlanningOrganizingWorkshipComponent', () => {
  let component: PlanningOrganizingWorkshipComponent;
  let fixture: ComponentFixture<PlanningOrganizingWorkshipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningOrganizingWorkshipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningOrganizingWorkshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
