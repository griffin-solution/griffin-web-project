import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-effective-disciplinary-management',
  templateUrl: './effective-disciplinary-management.component.html',
  styleUrls: ['./effective-disciplinary-management.component.sass']
})
export class EffectiveDisciplinaryManagementComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/effective-disciplinary-management.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/effective-disciplinary-management-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
