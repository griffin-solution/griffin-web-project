import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectiveDisciplinaryManagementComponent } from './effective-disciplinary-management.component';

describe('EffectiveDisciplinaryManagementComponent', () => {
  let component: EffectiveDisciplinaryManagementComponent;
  let fixture: ComponentFixture<EffectiveDisciplinaryManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EffectiveDisciplinaryManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectiveDisciplinaryManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
