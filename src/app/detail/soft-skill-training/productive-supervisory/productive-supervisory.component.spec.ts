import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductiveSupervisoryComponent } from './productive-supervisory.component';

describe('ProductiveSupervisoryComponent', () => {
  let component: ProductiveSupervisoryComponent;
  let fixture: ComponentFixture<ProductiveSupervisoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductiveSupervisoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductiveSupervisoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
