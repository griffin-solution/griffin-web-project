import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productive-supervisory',
  templateUrl: './productive-supervisory.component.html',
  styleUrls: ['./productive-supervisory.component.sass']
})
export class ProductiveSupervisoryComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/productive-supervisory-skills.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/productive-supervisory-skills-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
