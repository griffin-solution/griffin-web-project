import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TheNewUInOrgComponent } from './the-new-uin-org.component';

describe('TheNewUInOrgComponent', () => {
  let component: TheNewUInOrgComponent;
  let fixture: ComponentFixture<TheNewUInOrgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TheNewUInOrgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TheNewUInOrgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
