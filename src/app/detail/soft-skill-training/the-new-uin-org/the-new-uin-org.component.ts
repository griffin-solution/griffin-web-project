import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-the-new-uin-org',
  templateUrl: './the-new-uin-org.component.html',
  styleUrls: ['./the-new-uin-org.component.sass']
})
export class TheNewUInOrgComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/the-new-u-in-organization.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/the-new-u-in-organization-content.json").subscribe(data => {
      this.training = data;
    })
    console.clear()
  }
}

