import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageProgramComponent } from './language-program.component';

describe('LanguageProgramComponent', () => {
  let component: LanguageProgramComponent;
  let fixture: ComponentFixture<LanguageProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
