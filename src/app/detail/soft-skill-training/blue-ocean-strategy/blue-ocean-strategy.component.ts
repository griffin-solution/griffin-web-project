import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blue-ocean-strategy',
  templateUrl: './blue-ocean-strategy.component.html',
  styleUrls: ['./blue-ocean-strategy.component.sass']
})
export class BlueOceanStrategyComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/blue-ocean-program.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/blue-ocean-program-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
