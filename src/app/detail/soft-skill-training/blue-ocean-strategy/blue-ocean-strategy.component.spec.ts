import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueOceanStrategyComponent } from './blue-ocean-strategy.component';

describe('BlueOceanStrategyComponent', () => {
  let component: BlueOceanStrategyComponent;
  let fixture: ComponentFixture<BlueOceanStrategyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlueOceanStrategyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueOceanStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
