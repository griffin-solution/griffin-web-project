import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerialExecutiveDevelopmentComponent } from './managerial-executive-development.component';

describe('ManagerialExecutiveDevelopmentComponent', () => {
  let component: ManagerialExecutiveDevelopmentComponent;
  let fixture: ComponentFixture<ManagerialExecutiveDevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerialExecutiveDevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerialExecutiveDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
