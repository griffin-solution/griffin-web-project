import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KanbanInventoryComponent } from './kanban-inventory.component';

describe('KanbanInventoryComponent', () => {
  let component: KanbanInventoryComponent;
  let fixture: ComponentFixture<KanbanInventoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KanbanInventoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KanbanInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
