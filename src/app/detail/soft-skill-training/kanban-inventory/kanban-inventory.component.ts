import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kanban-inventory',
  templateUrl: './kanban-inventory.component.html',
  styleUrls: ['./kanban-inventory.component.sass']
})
export class KanbanInventoryComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/kanban-inventory-demand-management.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/kanban-inventory-demand-management-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
