import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositiveWorkingAttitudeComponent } from './positive-working-attitude.component';

describe('PositiveWorkingAttitudeComponent', () => {
  let component: PositiveWorkingAttitudeComponent;
  let fixture: ComponentFixture<PositiveWorkingAttitudeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositiveWorkingAttitudeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositiveWorkingAttitudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
