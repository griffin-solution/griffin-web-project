import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-positive-working-attitude',
  templateUrl: './positive-working-attitude.component.html',
  styleUrls: ['./positive-working-attitude.component.sass']
})
export class PositiveWorkingAttitudeComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/positive-working-attitude-for-peak-performance.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/soft-skill-training-detail/positive-working-attitude-for-peak-performance-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }
}
