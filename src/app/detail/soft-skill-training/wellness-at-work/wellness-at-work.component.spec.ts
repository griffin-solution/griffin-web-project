import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WellnessAtWorkComponent } from './wellness-at-work.component';

describe('WellnessAtWorkComponent', () => {
  let component: WellnessAtWorkComponent;
  let fixture: ComponentFixture<WellnessAtWorkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WellnessAtWorkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WellnessAtWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
