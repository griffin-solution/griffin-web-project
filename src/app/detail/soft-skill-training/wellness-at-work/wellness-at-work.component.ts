import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wellness-at-work',
  templateUrl: './wellness-at-work.component.html',
  styleUrls: ['./wellness-at-work.component.sass']
})
export class WellnessAtWorkComponent implements OnInit {
  contents: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/soft-skill-training-detail/wellness-at-work.json").subscribe(data => {
      this.contents = data;
    })

    console.clear()
  }
}
