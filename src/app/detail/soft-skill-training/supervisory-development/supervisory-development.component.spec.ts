import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisoryDevelopmentComponent } from './supervisory-development.component';

describe('SupervisoryDevelopmentComponent', () => {
  let component: SupervisoryDevelopmentComponent;
  let fixture: ComponentFixture<SupervisoryDevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupervisoryDevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisoryDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
