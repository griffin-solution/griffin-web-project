import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HrdIndustrialRelationsComponent } from './hrd-industrial-relations.component';

describe('HrdIndustrialRelationsComponent', () => {
  let component: HrdIndustrialRelationsComponent;
  let fixture: ComponentFixture<HrdIndustrialRelationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HrdIndustrialRelationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HrdIndustrialRelationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
