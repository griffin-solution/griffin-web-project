import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagingTrainingTrainerComponent } from './managing-training-trainer.component';

describe('ManagingTrainingTrainerComponent', () => {
  let component: ManagingTrainingTrainerComponent;
  let fixture: ComponentFixture<ManagingTrainingTrainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagingTrainingTrainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagingTrainingTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
