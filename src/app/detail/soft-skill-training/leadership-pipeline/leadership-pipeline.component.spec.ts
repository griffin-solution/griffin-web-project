import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadershipPipelineComponent } from './leadership-pipeline.component';

describe('LeadershipPipelineComponent', () => {
  let component: LeadershipPipelineComponent;
  let fixture: ComponentFixture<LeadershipPipelineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeadershipPipelineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadershipPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
