import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gamification',
  templateUrl: './gamification.component.html',
  styleUrls: ['./gamification.component.sass']
})
export class GamificationComponent implements OnInit {
  contents: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/gamification.json").subscribe(data => {
      this.contents = data;
    })

    console.clear();
  }

}
