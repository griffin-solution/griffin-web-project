import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplyChainSecurityTrainingComponent } from './supply-chain-security-training.component';

describe('SupplyChainSecurityTrainingComponent', () => {
  let component: SupplyChainSecurityTrainingComponent;
  let fixture: ComponentFixture<SupplyChainSecurityTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplyChainSecurityTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplyChainSecurityTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
