import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supply-chain-security-training',
  templateUrl: './supply-chain-security-training.component.html',
  styleUrls: ['./supply-chain-security-training.component.sass']
})
export class SupplyChainSecurityTrainingComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/supply-chain.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/technical-training-detail/supply-chain-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
