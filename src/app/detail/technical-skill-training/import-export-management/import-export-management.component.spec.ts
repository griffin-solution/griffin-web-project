import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExportManagementComponent } from './import-export-management.component';

describe('ImportExportManagementComponent', () => {
  let component: ImportExportManagementComponent;
  let fixture: ComponentFixture<ImportExportManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportExportManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExportManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
