import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustrialPlcTechComponent } from './industrial-plc-tech.component';

describe('IndustrialPlcTechComponent', () => {
  let component: IndustrialPlcTechComponent;
  let fixture: ComponentFixture<IndustrialPlcTechComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustrialPlcTechComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustrialPlcTechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
