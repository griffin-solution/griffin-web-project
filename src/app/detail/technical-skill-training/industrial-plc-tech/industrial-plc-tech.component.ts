import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-industrial-plc-tech',
  templateUrl: './industrial-plc-tech.component.html',
  styleUrls: ['./industrial-plc-tech.component.sass']
})
export class IndustrialPlcTechComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/industrial-plc.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/technical-training-detail/industrial-plc-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
