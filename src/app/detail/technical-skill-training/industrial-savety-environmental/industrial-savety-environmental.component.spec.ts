import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustrialSavetyEnvironmentalComponent } from './industrial-savety-environmental.component';

describe('IndustrialSavetyEnvironmentalComponent', () => {
  let component: IndustrialSavetyEnvironmentalComponent;
  let fixture: ComponentFixture<IndustrialSavetyEnvironmentalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndustrialSavetyEnvironmentalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustrialSavetyEnvironmentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
