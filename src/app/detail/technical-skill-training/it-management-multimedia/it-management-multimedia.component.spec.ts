import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItManagementMultimediaComponent } from './it-management-multimedia.component';

describe('ItManagementMultimediaComponent', () => {
  let component: ItManagementMultimediaComponent;
  let fixture: ComponentFixture<ItManagementMultimediaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItManagementMultimediaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItManagementMultimediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
