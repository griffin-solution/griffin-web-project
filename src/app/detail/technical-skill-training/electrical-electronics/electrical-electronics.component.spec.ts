import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricalElectronicsComponent } from './electrical-electronics.component';

describe('ElectricalElectronicsComponent', () => {
  let component: ElectricalElectronicsComponent;
  let fixture: ComponentFixture<ElectricalElectronicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricalElectronicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricalElectronicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
