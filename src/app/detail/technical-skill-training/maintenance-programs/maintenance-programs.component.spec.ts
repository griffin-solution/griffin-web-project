import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceProgramsComponent } from './maintenance-programs.component';

describe('MaintenanceProgramsComponent', () => {
  let component: MaintenanceProgramsComponent;
  let fixture: ComponentFixture<MaintenanceProgramsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceProgramsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
