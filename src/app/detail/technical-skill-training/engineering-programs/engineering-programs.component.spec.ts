import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineeringProgramsComponent } from './engineering-programs.component';

describe('EngineeringProgramsComponent', () => {
  let component: EngineeringProgramsComponent;
  let fixture: ComponentFixture<EngineeringProgramsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineeringProgramsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineeringProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
