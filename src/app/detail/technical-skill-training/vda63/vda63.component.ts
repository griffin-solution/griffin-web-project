import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vda63',
  templateUrl: './vda63.component.html',
  styleUrls: ['./vda63.component.sass']
})
export class Vda63Component implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/vda-63.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/technical-training-detail/vda-63-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
