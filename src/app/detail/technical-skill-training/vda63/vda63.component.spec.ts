import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Vda63Component } from './vda63.component';

describe('Vda63Component', () => {
  let component: Vda63Component;
  let fixture: ComponentFixture<Vda63Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Vda63Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Vda63Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
