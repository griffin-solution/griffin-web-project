import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Html5JavascriptProgrammingComponent } from './html5-javascript-programming.component';

describe('Html5JavascriptProgrammingComponent', () => {
  let component: Html5JavascriptProgrammingComponent;
  let fixture: ComponentFixture<Html5JavascriptProgrammingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Html5JavascriptProgrammingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Html5JavascriptProgrammingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
