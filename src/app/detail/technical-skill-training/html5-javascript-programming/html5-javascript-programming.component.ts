import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-html5-javascript-programming',
  templateUrl: './html5-javascript-programming.component.html',
  styleUrls: ['./html5-javascript-programming.component.sass']
})
export class Html5JavascriptProgrammingComponent implements OnInit {
  contents: any = [];
  training: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/html5css3.json").subscribe(data => {
      this.contents = data;
    })

    this.http.get("assets/json/technical-training-detail/html5css3-content.json").subscribe(data => {
      this.training = data;
    })

    console.clear();
  }

}
