import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rohs-hs',
  templateUrl: './rohs-hs.component.html',
  styleUrls: ['./rohs-hs.component.sass']
})
export class RohsHsComponent implements OnInit {
  contents: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/technical-training-detail/rohs-hs.json").subscribe(data => {
      this.contents = data;
    })

    console.clear();
  }

}
