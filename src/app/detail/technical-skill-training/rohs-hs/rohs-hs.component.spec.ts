import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RohsHsComponent } from './rohs-hs.component';

describe('RohsHsComponent', () => {
  let component: RohsHsComponent;
  let fixture: ComponentFixture<RohsHsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RohsHsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RohsHsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
