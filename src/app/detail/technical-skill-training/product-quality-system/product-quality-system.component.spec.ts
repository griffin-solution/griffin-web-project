import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductQualitySystemComponent } from './product-quality-system.component';

describe('ProductQualitySystemComponent', () => {
  let component: ProductQualitySystemComponent;
  let fixture: ComponentFixture<ProductQualitySystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductQualitySystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductQualitySystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
