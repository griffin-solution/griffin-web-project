import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationManagementLogisticComponent } from './operation-management-logistic.component';

describe('OperationManagementLogisticComponent', () => {
  let component: OperationManagementLogisticComponent;
  let fixture: ComponentFixture<OperationManagementLogisticComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OperationManagementLogisticComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationManagementLogisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
