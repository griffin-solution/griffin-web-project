import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-recognition-certification',
  templateUrl: './recognition-certification.component.html',
  styleUrls: ['./recognition-certification.component.sass']
})
export class RecognitionCertificationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const cert = $('.certificate');
    cert.magnificPopup({
      delegate: 'a.mag-popup',
      type: 'image',
      removalDelay: 300,
      gallery:{
          enabled:true,
      },
      mainClass: 'mfp-with-zoom',
      zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          // tslint:disable-next-line:typedef
          opener(openerElement: { is: (arg0: string) => any; find: (arg0: string) => any; }) {
              return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
      }
    });
  }

}
