import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecognitionCertificationComponent } from './recognition-certification.component';

describe('RecognitionCertificationComponent', () => {
  let component: RecognitionCertificationComponent;
  let fixture: ComponentFixture<RecognitionCertificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecognitionCertificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecognitionCertificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
