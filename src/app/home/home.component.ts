import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import * as AOS from 'aos';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
  animations: [
    trigger('fade',
    [
      state('void', style({ opacity : 0})),
      transition(':enter', [ animate(300)]),
      transition(':leave', [ animate(500)]),
    ]
  )]
})
export class HomeComponent implements OnInit {
  isShow = false;
  topPosToStartShowing = 100;

  constructor() { }

  // tslint:disable-next-line:typedef
  ngOnInit(){
    AOS.init();

    $('#myMclosedal').on('hidden.bs.modal', function () {
      var video = $("#vid1").attr("src");
      var video2 = $("#vid2").attr("src");
      var video3 = $("#vid3").attr("src");

      $("#vid1").attr("src","");
      $("#vid1").attr("src",video);

      $("#vid2").attr("src","");
      $("#vid2").attr("src",video);

      $("#vid3").attr("src","");
      $("#vid3").attr("src",video);
    })

    //video control
    $('.vids1').prop("controls", false);
    $('.vids2').prop("controls", false);
    $('.vids3').prop("controls", false);

    $(".vids1").hover(function() {
      $('.vids1').prop("controls", true);
    }, function() {
      $('.vids1').prop("controls", false);
    });

    $(".vids2").hover(function() {
      $('.vids2').prop("controls", true);
    }, function() {
      $('.vids2').prop("controls", false);
    });

    $(".vids3").hover(function() {
      $('.vids3').prop("controls", true);
    }, function() {
      $('.vids3').prop("controls", false);
    });
  }

  @HostListener('window:scroll')
  // tslint:disable-next-line:typedef
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    // console.log('[scroll]', scrollPosition);
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  // tslint:disable-next-line:typedef
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
