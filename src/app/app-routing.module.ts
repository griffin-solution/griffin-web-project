import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { CeoMessageComponent } from './ceo-message/ceo-message.component';
import { CommitmentComponent } from './commitment/commitment.component';
import { ContactComponent } from './contact/contact.component';
import { CorporateInformationComponent } from './corporate-information/corporate-information.component';
import { CorporateStatementComponent } from './corporate-statement/corporate-statement.component';
import { CustomerComponent } from './customer/customer.component';
import { AutonomousMaintenanceComponent } from './detail/industrial-training/autonomous-maintenance/autonomous-maintenance.component';
import { BusinessAllianceComponent } from './detail/industrial-training/business-alliance/business-alliance.component';
import { ContractReviewManagementComponent } from './detail/industrial-training/contract-review-management/contract-review-management.component';
import { HalalAwarnessComponent } from './detail/industrial-training/halal-awarness/halal-awarness.component';
import { HalalFsscComponent } from './detail/industrial-training/halal-fssc/halal-fssc.component';
import { HalalIndustryComponent } from './detail/industrial-training/halal-industry/halal-industry.component';
import { HidraulicsComponent } from './detail/industrial-training/hidraulics/hidraulics.component';
import { HygieneFoodComponent } from './detail/industrial-training/hygiene-food/hygiene-food.component';
import { OilGasComponent } from './detail/industrial-training/oil-gas/oil-gas.component';
import { PlcComponent } from './detail/industrial-training/plc/plc.component';
import { UnderstandingMechanicalElementComponent } from './detail/industrial-training/understanding-mechanical-element/understanding-mechanical-element.component';
import { BlueOceanStrategyComponent } from './detail/soft-skill-training/blue-ocean-strategy/blue-ocean-strategy.component';
import { CommunicationHrComponent } from './detail/soft-skill-training/communication-hr/communication-hr.component';
import { DesignThinkingWorkshopComponent } from './detail/soft-skill-training/design-thinking-workshop/design-thinking-workshop.component';
import { EffectiveBusinessEnglishWritingComponent } from './detail/soft-skill-training/effective-business-english-writing/effective-business-english-writing.component';
import { EffectiveDisciplinaryManagementComponent } from './detail/soft-skill-training/effective-disciplinary-management/effective-disciplinary-management.component';
import { EmployeeDevelopmentComponent } from './detail/soft-skill-training/employee-development/employee-development.component';
import { FinancialManagementComponent } from './detail/soft-skill-training/financial-management/financial-management.component';
import { HrdIndustrialRelationsComponent } from './detail/soft-skill-training/hrd-industrial-relations/hrd-industrial-relations.component';
import { KanbanInventoryComponent } from './detail/soft-skill-training/kanban-inventory/kanban-inventory.component';
import { LanguageProgramComponent } from './detail/soft-skill-training/language-program/language-program.component';
import { LeadershipPipelineComponent } from './detail/soft-skill-training/leadership-pipeline/leadership-pipeline.component';
import { ManagementComponent } from './detail/soft-skill-training/management/management.component';
import { ManagerialExecutiveDevelopmentComponent } from './detail/soft-skill-training/managerial-executive-development/managerial-executive-development.component';
import { ManagingEmotionalIntelligenceComponent } from './detail/soft-skill-training/managing-emotional-intelligence/managing-emotional-intelligence.component';
import { ManagingTrainingTrainerComponent } from './detail/soft-skill-training/managing-training-trainer/managing-training-trainer.component';
import { PlanningOrganizingWorkshipComponent } from './detail/soft-skill-training/planning-organizing-workship/planning-organizing-workship.component';
import { PositiveWorkingAttitudeComponent } from './detail/soft-skill-training/positive-working-attitude/positive-working-attitude.component';
import { ProductiveSupervisoryComponent } from './detail/soft-skill-training/productive-supervisory/productive-supervisory.component';
import { SalesMarketingComponent } from './detail/soft-skill-training/sales-marketing/sales-marketing.component';
import { Sharepoint365Component } from './detail/soft-skill-training/sharepoint365/sharepoint365.component';
import { SpeakingForSuccessComponent } from './detail/soft-skill-training/speaking-for-success/speaking-for-success.component';
import { StoryTellingComponent } from './detail/soft-skill-training/story-telling/story-telling.component';
import { SupervisoryDevelopmentComponent } from './detail/soft-skill-training/supervisory-development/supervisory-development.component';
import { TeamBuildingComponent } from './detail/soft-skill-training/team-building/team-building.component';
import { TheNewUInOrgComponent } from './detail/soft-skill-training/the-new-uin-org/the-new-uin-org.component';
import { TrademarkProgramComponent } from './detail/soft-skill-training/trademark-program/trademark-program.component';
import { UnleashYourPotentialComponent } from './detail/soft-skill-training/unleash-your-potential/unleash-your-potential.component';
import { WellnessAtWorkComponent } from './detail/soft-skill-training/wellness-at-work/wellness-at-work.component';
import { BankingComponent } from './detail/technical-skill-training/banking/banking.component';
import { ElectricalElectronicsComponent } from './detail/technical-skill-training/electrical-electronics/electrical-electronics.component';
import { EngineeringProgramsComponent } from './detail/technical-skill-training/engineering-programs/engineering-programs.component';
import { GamificationComponent } from './detail/technical-skill-training/gamification/gamification.component';
import { Html5JavascriptProgrammingComponent } from './detail/technical-skill-training/html5-javascript-programming/html5-javascript-programming.component';
import { ImportExportManagementComponent } from './detail/technical-skill-training/import-export-management/import-export-management.component';
import { IndustrialPlcTechComponent } from './detail/technical-skill-training/industrial-plc-tech/industrial-plc-tech.component';
import { IndustrialSavetyEnvironmentalComponent } from './detail/technical-skill-training/industrial-savety-environmental/industrial-savety-environmental.component';
import { IotComponent } from './detail/technical-skill-training/iot/iot.component';
import { ItManagementMultimediaComponent } from './detail/technical-skill-training/it-management-multimedia/it-management-multimedia.component';
import { MaintenanceProgramsComponent } from './detail/technical-skill-training/maintenance-programs/maintenance-programs.component';
import { OperationManagementLogisticComponent } from './detail/technical-skill-training/operation-management-logistic/operation-management-logistic.component';
import { OperationManagementComponent } from './detail/technical-skill-training/operation-management/operation-management.component';
import { ProductQualitySystemComponent } from './detail/technical-skill-training/product-quality-system/product-quality-system.component';
import { RohsHsComponent } from './detail/technical-skill-training/rohs-hs/rohs-hs.component';
import { SupplyChainSecurityTrainingComponent } from './detail/technical-skill-training/supply-chain-security-training/supply-chain-security-training.component';
import { Vda63Component } from './detail/technical-skill-training/vda63/vda63.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { IndustrialBaseTrainingComponent } from './industrial-base-training/industrial-base-training.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { IsoStandardComponent } from './iso-standard/iso-standard.component';
import { ProgramsAvailableComponent } from './programs-available/programs-available.component';
import { RecognitionCertificationComponent } from './recognition-certification/recognition-certification.component';
import { SoftSkillTrainingComponent } from './soft-skill-training/soft-skill-training.component';
import { TechnicalSkillTrainingComponent } from './technical-skill-training/technical-skill-training.component';
import { TrainingsComponent } from './trainings/trainings.component';
import { VisionComponent } from './vision/vision.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'introduction',
    component: IntroductionComponent
  },
  {
    path: 'about-us',
    component: AboutComponent
  },
  {
    path: 'our-vision-and-mission',
    component: VisionComponent
  },
  {
    path: 'our-commitment',
    component: CommitmentComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'ceo-message',
    component: CeoMessageComponent
  },
  {
    path: 'corporate-statement',
    component: CorporateStatementComponent
  },
  {
    path: 'corporate-information',
    component: CorporateInformationComponent
  },
  {
    path: 'soft-skill-training',
    component: SoftSkillTrainingComponent
  },
  {
    path: 'technical-skill-training',
    component: TechnicalSkillTrainingComponent
  },
  {
    path: 'industrial-base-training',
    component: IndustrialBaseTrainingComponent
  },
  {
    path: 'iso-standard-and-social-compliance',
    component: IsoStandardComponent
  },
  {
    path: 'recognition-certifiation',
    component: RecognitionCertificationComponent
  },
  {
    path: 'customers',
    component: CustomerComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'available-programs',
    component: ProgramsAvailableComponent
  },
  {
    path: 'trainings',
    component: TrainingsComponent
  },

  //detail page for soft skill training
  // {
  //   path: 'soft-skill-training/detail/communication-hr-training-programs',
  //   component: CommunicationHrComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/employee-development',
  //   component: EmployeeDevelopmentComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/hrd-and-industrial-relations',
  //   component: HrdIndustrialRelationsComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/language-programs',
  //   component: LanguageProgramComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/leadership-pipeline-programs',
  //   component: LeadershipPipelineComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/management',
  //   component: ManagementComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/managerial-and-executive-development',
  //   component: ManagerialExecutiveDevelopmentComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/managing-training-and-trainer-programs',  ///sudah
  //   component: ManagingTrainingTrainerComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/sales-and-marketing-programs',
  //   component: SalesMarketingComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/supervisory-development-programs',
  //   component: SupervisoryDevelopmentComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/team-building',
  //   component: TeamBuildingComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/trademark-program',
  //   component: TrademarkProgramComponent
  // },
  // {
  //   path: 'soft-skill-training/detail/financial-management-programs',
  //   component: FinancialManagementComponent
  // },
  //New
    {
      path: 'soft-skill-training/detail/blue-ocean-strategy-program',
      component: BlueOceanStrategyComponent
    },
    {
      path: 'soft-skill-training/detail/design-thinking-workshop',
      component: DesignThinkingWorkshopComponent
    },
    {
      path: 'soft-skill-training/detail/effective-bussiness-english-writing',
      component: EffectiveBusinessEnglishWritingComponent
    },
    {
      path: 'soft-skill-training/detail/effective-disciplinary-management-on-misconduct',
      component: EffectiveDisciplinaryManagementComponent
    },
    {
      path: 'soft-skill-training/detail/kanban-inventory-demand-management',
      component: KanbanInventoryComponent
    },
    {
      path: 'soft-skill-training/detail/managing-emotional-intelligence',
      component: ManagingEmotionalIntelligenceComponent
    },
    {
      path: 'soft-skill-training/detail/planning-and-organizing-workshop',
      component: PlanningOrganizingWorkshipComponent
    },
    {
      path: 'soft-skill-training/detail/positive-working-attitude-for-peak-performance',
      component: PositiveWorkingAttitudeComponent
    },
    {
      path: 'soft-skill-training/detail/productive-supervisory-skills',
      component: ProductiveSupervisoryComponent
    },
    {
      path: 'soft-skill-training/detail/sharepoint-365-end-user-training',
      component: Sharepoint365Component
    },
    {
      path: 'soft-skill-training/detail/speaking-for-success',
      component: SpeakingForSuccessComponent
    },
    {
      path: 'soft-skill-training/detail/story-telling-for-influence-and-communication-skills',
      component: StoryTellingComponent
    },
    {
      path: 'soft-skill-training/detail/the-new-u-in-organization',
      component: TheNewUInOrgComponent
    },
    {
      path: 'soft-skill-training/detail/unleash-your-potential',
      component: UnleashYourPotentialComponent
    },
    {
      path: 'soft-skill-training/detail/wellness-at-work',
      component: WellnessAtWorkComponent
    },
  //techical skill
  // {
  //   path: 'technical-skill-training/detail/iot',
  //   component: IotComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/product-quality-system-management',
  //   component: ProductQualitySystemComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/operation-management',
  //   component: OperationManagementComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/operation-management-logistics',
  //   component: OperationManagementLogisticComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/import-export-management',
  //   component: ImportExportManagementComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/maintenance-programs',
  //   component: MaintenanceProgramsComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/electronical-electronics',
  //   component: ElectricalElectronicsComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/industrial-safety-environtmental',
  //   component: IndustrialSavetyEnvironmentalComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/it-management-and-multimedia',
  //   component: ItManagementMultimediaComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/banking',
  //   component: BankingComponent
  // },
  // {
  //   path: 'technical-skill-training/detail/engineering-programs',
  //   component: EngineeringProgramsComponent
  // },

  //new
  {
    path: 'technical-skill-training/detail/html5-javascript-programming',
    component: Html5JavascriptProgrammingComponent
  },
  {
    path: 'technical-skill-training/detail/gamification',
    component: GamificationComponent
  },
  {
    path: 'technical-skill-training/detail/industrial-plc-technology-operation',
    component: IndustrialPlcTechComponent
  },
  {
    path: 'technical-skill-training/detail/rohs-hs-management-system',
    component: RohsHsComponent
  },
  {
    path: 'technical-skill-training/detail/supply-chain-security-training',
    component: SupplyChainSecurityTrainingComponent
  },
  {
    path: 'technical-skill-training/detail/vda-6.3-implementing-the-process-audit',
    component: Vda63Component
  },

  //industrial training
  // {
  //   path: 'industrial-base-training/detail/pls-scada',
  //   component: PlcComponent
  // },
  // {
  //   path: 'industrial-base-training/detail/oil-gas',
  //   component: OilGasComponent
  // },
  // {
  //   path: 'industrial-base-training/detail/hygiene-food-safety',
  //   component: HygieneFoodComponent
  // },
  //new
  {
    path: 'industrial-base-training/detail/autonomous-maintenance',
    component: AutonomousMaintenanceComponent
  },
  {
    path: 'industrial-base-training/detail/best-practice-for-halal-industry',
    component: HalalIndustryComponent
  },
  {
    path: 'industrial-base-training/detail/contract-review-and-management',
    component: ContractReviewManagementComponent
  },
  {
    path: 'industrial-base-training/detail/halal-awarness',
    component: HalalAwarnessComponent
  },
  {
    path: 'industrial-base-training/detail/industrial-pneumatics',
    component: HidraulicsComponent
  },
  {
    path: 'industrial-base-training/detail/integrated-haccp-halal-fscc',
    component: HalalFsscComponent
  },
  {
    path: 'industrial-base-training/detail/understanding-implementation-of-responsible-business-alliance',
    component: BusinessAllianceComponent
  },
  {
    path: 'industrial-base-training/detail/understanding-of-mechanical-elements',
    component: UnderstandingMechanicalElementComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
