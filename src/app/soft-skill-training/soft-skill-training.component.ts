import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-soft-skill-training',
  templateUrl: './soft-skill-training.component.html',
  styleUrls: ['./soft-skill-training.component.sass']
})
export class SoftSkillTrainingComponent implements OnInit {
  quotes: any = [];
  randomQuote: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("assets/json/quotes.json").subscribe(data => {
      this.quotes = data;
      this.randomQuote = this.quotes[Math.floor(Math.random() * this.quotes.length)];
    })

    $('a').click(() => {
      console.log('link clicked');
      $('a').addClass("visited");
    });

    console.clear();
    // $('a').click(function(){
    //   console.log('link clicked');

    //   $('a').addClass("visited");
    // });
  }

}
