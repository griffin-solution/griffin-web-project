import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftSkillTrainingComponent } from './soft-skill-training.component';

describe('SoftSkillTrainingComponent', () => {
  let component: SoftSkillTrainingComponent;
  let fixture: ComponentFixture<SoftSkillTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoftSkillTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftSkillTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
