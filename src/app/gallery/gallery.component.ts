import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.sass']
})
export class GalleryComponent implements OnInit {
  galleries: any = [];
  p: number = 1;
  itemPerpage: number = 12;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.loadGallery()

    const cert = $('.galleries');
    cert.magnificPopup({
      delegate: 'a.mag-popup',
      type: 'image',
      removalDelay: 300,
      gallery:{
          enabled:true,
      },
      mainClass: 'mfp-with-zoom',
      zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          // tslint:disable-next-line:typedef
          opener(openerElement: { is: (arg0: string) => any; find: (arg0: string) => any; }) {
              return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
      }
    });

    $('.mag-popup').click(() => {
      console.log('mag popup clicked');

    })
  }

  loadGallery(){
    this.http.get('assets/json/gallery.json').subscribe(data => {
      this.galleries = data;
      console.log(this.galleries);

    })
  }

}
