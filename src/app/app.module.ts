import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
//angular-calendar
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FormsModule } from '@angular/forms';

//videogular
import {VgCoreModule} from '@videogular/ngx-videogular/core';
import {VgControlsModule} from '@videogular/ngx-videogular/controls';
import {VgBufferingModule} from '@videogular/ngx-videogular/buffering';
import {VgOverlayPlayModule} from '@videogular/ngx-videogular/overlay-play';

import { NgxPaginationModule } from 'ngx-pagination';

// component
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GriffinTrainingService } from './services/griffin-training.service';
import { FooterComponent } from './footer/footer.component';
import { FabComponent } from './fab/fab.component';
import { AboutComponent } from './about/about.component';
import { VisionComponent } from './vision/vision.component';
import { MissionComponent } from './mission/mission.component';
import { CommitmentComponent } from './commitment/commitment.component';
import { CeoMessageComponent } from './ceo-message/ceo-message.component';
import { CorporateStatementComponent } from './corporate-statement/corporate-statement.component';
import { CorporateInformationComponent } from './corporate-information/corporate-information.component';
import { SoftSkillTrainingComponent } from './soft-skill-training/soft-skill-training.component';
import { TechnicalSkillTrainingComponent } from './technical-skill-training/technical-skill-training.component';
import { IndustrialBaseTrainingComponent } from './industrial-base-training/industrial-base-training.component';
import { IsoStandardComponent } from './iso-standard/iso-standard.component';
import { RecognitionCertificationComponent } from './recognition-certification/recognition-certification.component';
import { CustomerComponent } from './customer/customer.component';
import { ContactComponent } from './contact/contact.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { ProgramsAvailableComponent } from './programs-available/programs-available.component';
import { ManagementComponent } from './detail/soft-skill-training/management/management.component';
import { TrademarkProgramComponent } from './detail/soft-skill-training/trademark-program/trademark-program.component';
import { LeadershipPipelineComponent } from './detail/soft-skill-training/leadership-pipeline/leadership-pipeline.component';
import { CommunicationHrComponent } from './detail/soft-skill-training/communication-hr/communication-hr.component';
import { ManagingTrainingTrainerComponent } from './detail/soft-skill-training/managing-training-trainer/managing-training-trainer.component';
import { TeamBuildingComponent } from './detail/soft-skill-training/team-building/team-building.component';
import { EmployeeDevelopmentComponent } from './detail/soft-skill-training/employee-development/employee-development.component';
import { HrdIndustrialRelationsComponent } from './detail/soft-skill-training/hrd-industrial-relations/hrd-industrial-relations.component';
import { ManagerialExecutiveDevelopmentComponent } from './detail/soft-skill-training/managerial-executive-development/managerial-executive-development.component';
import { SupervisoryDevelopmentComponent } from './detail/soft-skill-training/supervisory-development/supervisory-development.component';
import { LanguageProgramComponent } from './detail/soft-skill-training/language-program/language-program.component';
import { SalesMarketingComponent } from './detail/soft-skill-training/sales-marketing/sales-marketing.component';
import { IotComponent } from './detail/technical-skill-training/iot/iot.component';
import { FinancialManagementComponent } from './detail/soft-skill-training/financial-management/financial-management.component';
import { ProductQualitySystemComponent } from './detail/technical-skill-training/product-quality-system/product-quality-system.component';
import { OperationManagementComponent } from './detail/technical-skill-training/operation-management/operation-management.component';
import { OperationManagementLogisticComponent } from './detail/technical-skill-training/operation-management-logistic/operation-management-logistic.component';
import { ImportExportManagementComponent } from './detail/technical-skill-training/import-export-management/import-export-management.component';
import { EngineeringProgramsComponent } from './detail/technical-skill-training/engineering-programs/engineering-programs.component';
import { MaintenanceProgramsComponent } from './detail/technical-skill-training/maintenance-programs/maintenance-programs.component';
import { ElectricalElectronicsComponent } from './detail/technical-skill-training/electrical-electronics/electrical-electronics.component';
import { IndustrialSavetyEnvironmentalComponent } from './detail/technical-skill-training/industrial-savety-environmental/industrial-savety-environmental.component';
import { ItManagementMultimediaComponent } from './detail/technical-skill-training/it-management-multimedia/it-management-multimedia.component';
import { BankingComponent } from './detail/technical-skill-training/banking/banking.component';
import { PlcComponent } from './detail/industrial-training/plc/plc.component';
import { HidraulicsComponent } from './detail/industrial-training/hidraulics/hidraulics.component';
import { OilGasComponent } from './detail/industrial-training/oil-gas/oil-gas.component';
import { HygieneFoodComponent } from './detail/industrial-training/hygiene-food/hygiene-food.component';
import { GalleryComponent } from './gallery/gallery.component';
import { AutonomousMaintenanceComponent } from './detail/industrial-training/autonomous-maintenance/autonomous-maintenance.component';
import { HalalIndustryComponent } from './detail/industrial-training/halal-industry/halal-industry.component';
import { ContractReviewManagementComponent } from './detail/industrial-training/contract-review-management/contract-review-management.component';
import { HalalAwarnessComponent } from './detail/industrial-training/halal-awarness/halal-awarness.component';
import { HalalFsscComponent } from './detail/industrial-training/halal-fssc/halal-fssc.component';
import { BusinessAllianceComponent } from './detail/industrial-training/business-alliance/business-alliance.component';
import { UnderstandingMechanicalElementComponent } from './detail/industrial-training/understanding-mechanical-element/understanding-mechanical-element.component';
import { Html5JavascriptProgrammingComponent } from './detail/technical-skill-training/html5-javascript-programming/html5-javascript-programming.component';
import { GamificationComponent } from './detail/technical-skill-training/gamification/gamification.component';
import { IndustrialPlcTechComponent } from './detail/technical-skill-training/industrial-plc-tech/industrial-plc-tech.component';
import { RohsHsComponent } from './detail/technical-skill-training/rohs-hs/rohs-hs.component';
import { SupplyChainSecurityTrainingComponent } from './detail/technical-skill-training/supply-chain-security-training/supply-chain-security-training.component';
import { Vda63Component } from './detail/technical-skill-training/vda63/vda63.component';
import { BlueOceanStrategyComponent } from './detail/soft-skill-training/blue-ocean-strategy/blue-ocean-strategy.component';
import { DesignThinkingWorkshopComponent } from './detail/soft-skill-training/design-thinking-workshop/design-thinking-workshop.component';
import { EffectiveBusinessEnglishWritingComponent } from './detail/soft-skill-training/effective-business-english-writing/effective-business-english-writing.component';
import { EffectiveDisciplinaryManagementComponent } from './detail/soft-skill-training/effective-disciplinary-management/effective-disciplinary-management.component';
import { KanbanInventoryComponent } from './detail/soft-skill-training/kanban-inventory/kanban-inventory.component';
import { ManagingEmotionalIntelligenceComponent } from './detail/soft-skill-training/managing-emotional-intelligence/managing-emotional-intelligence.component';
import { PlanningOrganizingWorkshipComponent } from './detail/soft-skill-training/planning-organizing-workship/planning-organizing-workship.component';
import { PositiveWorkingAttitudeComponent } from './detail/soft-skill-training/positive-working-attitude/positive-working-attitude.component';
import { ProductiveSupervisoryComponent } from './detail/soft-skill-training/productive-supervisory/productive-supervisory.component';
import { Sharepoint365Component } from './detail/soft-skill-training/sharepoint365/sharepoint365.component';
import { SpeakingForSuccessComponent } from './detail/soft-skill-training/speaking-for-success/speaking-for-success.component';
import { StoryTellingComponent } from './detail/soft-skill-training/story-telling/story-telling.component';
import { TheNewUInOrgComponent } from './detail/soft-skill-training/the-new-uin-org/the-new-uin-org.component';
import { UnleashYourPotentialComponent } from './detail/soft-skill-training/unleash-your-potential/unleash-your-potential.component';
import { WellnessAtWorkComponent } from './detail/soft-skill-training/wellness-at-work/wellness-at-work.component';
import { TrainingsComponent } from './trainings/trainings.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    FabComponent,
    AboutComponent,
    VisionComponent,
    MissionComponent,
    CommitmentComponent,
    CeoMessageComponent,
    CorporateStatementComponent,
    CorporateInformationComponent,
    SoftSkillTrainingComponent,
    TechnicalSkillTrainingComponent,
    IndustrialBaseTrainingComponent,
    IsoStandardComponent,
    RecognitionCertificationComponent,
    CustomerComponent,
    ContactComponent,
    IntroductionComponent,
    ProgramsAvailableComponent,
    ManagementComponent,
    TrademarkProgramComponent,
    LeadershipPipelineComponent,
    CommunicationHrComponent,
    ManagingTrainingTrainerComponent,
    TeamBuildingComponent,
    EmployeeDevelopmentComponent,
    HrdIndustrialRelationsComponent,
    ManagerialExecutiveDevelopmentComponent,
    SupervisoryDevelopmentComponent,
    LanguageProgramComponent,
    SalesMarketingComponent,
    IotComponent,
    FinancialManagementComponent,
    ProductQualitySystemComponent,
    OperationManagementComponent,
    OperationManagementLogisticComponent,
    ImportExportManagementComponent,
    EngineeringProgramsComponent,
    MaintenanceProgramsComponent,
    ElectricalElectronicsComponent,
    IndustrialSavetyEnvironmentalComponent,
    ItManagementMultimediaComponent,
    BankingComponent,
    PlcComponent,
    HidraulicsComponent,
    OilGasComponent,
    HygieneFoodComponent,
    GalleryComponent,
    AutonomousMaintenanceComponent,
    HalalIndustryComponent,
    ContractReviewManagementComponent,
    HalalAwarnessComponent,
    HalalFsscComponent,
    BusinessAllianceComponent,
    UnderstandingMechanicalElementComponent,
    Html5JavascriptProgrammingComponent,
    GamificationComponent,
    IndustrialPlcTechComponent,
    RohsHsComponent,
    SupplyChainSecurityTrainingComponent,
    Vda63Component,
    BlueOceanStrategyComponent,
    DesignThinkingWorkshopComponent,
    EffectiveBusinessEnglishWritingComponent,
    EffectiveDisciplinaryManagementComponent,
    KanbanInventoryComponent,
    ManagingEmotionalIntelligenceComponent,
    PlanningOrganizingWorkshipComponent,
    PositiveWorkingAttitudeComponent,
    ProductiveSupervisoryComponent,
    Sharepoint365Component,
    SpeakingForSuccessComponent,
    StoryTellingComponent,
    TheNewUInOrgComponent,
    UnleashYourPotentialComponent,
    WellnessAtWorkComponent,
    TrainingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    NgbModule,
    FormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    NgxPaginationModule
  ],
  providers: [GriffinTrainingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
