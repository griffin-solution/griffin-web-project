import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.sass']
})
export class FabComponent implements OnInit {

  constructor(private http: HttpClient) { }
  chatMessage: any = [];
  chatData: any = [];
  chatLength: any;
  dataposition: any;
  haveOptions: any;
  arrOPtion: any = [];
  firstID: any = [];
  lastIndex: any;

  ngOnInit(): void {
    this.displayBox();
    this.fabOnClick();
    // this.clickOPtion();

    $.get('assets/json/cs_chat.json', (data:any) => {
      this.chatMessage = data;
      this.chatLength = data.length;

      // console.log('chat data', this.chatMessage);
      // console.log('chat length', this.chatLength);

      for (let i = 0; i < this.chatLength; i++) {
        this.dataposition = data[i];
        this.haveOptions = this.dataposition.hasOwnProperty("options");

        if(this.haveOptions == true){
          this.arrOPtion.push(this.dataposition.id);
          this.firstID = this.arrOPtion[0];
        }
        // console.log(this.dataposition);
      }
      this.lastIndex = this.arrOPtion.pop();
      this.loopData(this.firstID, this.dataposition, this.chatMessage, this.lastIndex);
    })
  }

  loopData(firstID: any, dataPos: any, chatData: any, lastIndex: any){
    // console.log('firstID', firstID);
    // console.log('dataPos', dataPos);
    // console.log('chatData', chatData);
    // console.log('lastIndex', lastIndex);

    for (let i = 0; i < lastIndex; i++) {
      this.typingAnimation();
      $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
        dataPos = chatData[i];
        // append chat
        var appendQuestion =
        `
          <div class="justify-content-start">
            <div class="col-12 pl-0 pr-0">
              <div class="message-wrapper">
                <div class="message">
                  <div class="text">
                    <p class="mt-2 mb-0 message-text"
                    style="
                    max-width: 100%;
                    border-bottom-right-radius: 20px;
                    border-top-right-radius: 20px;
                    border-top-left-radius: 20px;
                    position: relative;
                    display: inline-block;
                    background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                    width: -webkit-fit-content;
                    width: -moz-fit-content;
                    width: fit-content;
                    padding: 10px;">
                      `+dataPos.message+`
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        `;
        $(appendQuestion).appendTo(".chat-box-container");
        // console.log('firstID-2', firstID-1);

        if(i == firstID-1){
          // console.log('lastID appear');
          $(".action-button").delay(500).fadeIn(() => {
            let optionData = chatData[firstID-1]['options'];

            $.each(optionData, (i: any, val: { message: string; }) => {
              console.log('val', val);
              // let options = `<button type="button" class="btn btn-light btn-block opt-btn" (click)="displayOption()">`+val.message+`</button>`;
              // $(".imessage").append(options);
              $(".choices").css('display', 'block');
              $(".opt-btn").addClass('option_clicked');
            })

            $(".chat-box").css({
              'top': '-26%'
            });
          })
        }
      })
    }
  }

  typingAnimation(){
    $(".typing").css({
      "display" : 'flex',
      "margin-bottom" : '5px'
    })

    $(".col-typing-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut();
    console.log('typing animation');
  }

  displayBox(){
    $(".box").show(700)
  }

  fabOnClick(){
    $(".btn-floating").click(() => {
      $(".box").fadeToggle("slow")
      $("this").find("i.icon").toggleClass("far fa-comment-alt").toggleClass("fas fa-chevron-down")
      $(".conversation-box").fadeIn('slow')
    })
  }

  technicalSkillClick(){
    console.log('Technical Skill clicked');
    $(".choices").css('display', 'none');

    //append message
    var appendAnswer =
    `
    <div class="row pr5 justify-content-end">
        <div class="col-8 pl-0 pr-0">
            <div class="reply float-right" style="background-color: #fff;
              border-bottom-left-radius: 20px;
              border-top-left-radius: 20px;
              border-top-right-radius: 20px;
              display: inline-block;
              margin-top: .5rem;
              max-width: 100%;
              position: relative;
              margin-right: 15px;">
              <div class="text text-left" style="padding: 10px;">
                I want to ask about Technical Skill Training
              </div>
            </div>
        </div>
    </div>
    `;
    $(appendAnswer).appendTo(".chat-box-container");

    $.get('assets/json/technicalSkillSelected.json', (data:any) => {
      this.chatMessage = data;
      this.chatLength = data.length;

      for (let i = 0; i < this.chatLength-1; i++) {
        this.typingAnimation();
        $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
          let chatData = this.chatMessage[i];


          var appendQuestion =
          `
            <div class="justify-content-start">
              <div class="col-12 pl-0 pr-0">
                <div class="message-wrapper">
                  <div class="message">
                    <div class="text">
                      <p class="mt-2 mb-0 message-text"
                      style="
                      max-width: 100%;
                      border-bottom-right-radius: 20px;
                      border-top-right-radius: 20px;
                      border-top-left-radius: 20px;
                      position: relative;
                      display: inline-block;
                      background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                      width: -webkit-fit-content;
                      width: -moz-fit-content;
                      width: fit-content;
                      padding: 10px;">
                        `+chatData.message+`
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `;
          $(appendQuestion).appendTo(".chat-box-container");

          for (let i = 1; i < this.chatLength; i++) {
            this.typingAnimation();
            $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
              let chatData = this.chatMessage[i];


              var appendQuestion =
            `
              <div class="justify-content-start">
                <div class="col-12 pl-0 pr-0">
                  <div class="message-wrapper">
                    <div class="message">
                      <div class="text">
                        <p class="mt-2 mb-0 message-text"
                        style="
                        max-width: 100%;
                        border-bottom-right-radius: 20px;
                        border-top-right-radius: 20px;
                        border-top-left-radius: 20px;
                        position: relative;
                        display: inline-block;
                        background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                        width: -webkit-fit-content;
                        width: -moz-fit-content;
                        width: fit-content;
                        padding: 10px;">
                          <a href='`+chatData.message+`' target="_blank">`+chatData.message+`<a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            `;
            $(appendQuestion).appendTo(".chat-box-container");
            })
          }
        })
      }
    })
  }

  softSkillClick(){
    console.log('Soft Skill clicked');
    $(".choices").css('display', 'none');

    //append message
    var appendAnswer =
    `
    <div class="row pr5 justify-content-end">
        <div class="col-8 pl-0 pr-0">
            <div class="reply float-right" style="background-color: #fff;
              border-bottom-left-radius: 20px;
              border-top-left-radius: 20px;
              border-top-right-radius: 20px;
              display: inline-block;
              margin-top: .5rem;
              max-width: 100%;
              position: relative;
              margin-right: 15px;">
              <div class="text text-left" style="padding: 10px;">
                I want to ask about Soft Skill Training
              </div>
            </div>
        </div>
    </div>
    `;
    $(appendAnswer).appendTo(".chat-box-container");

    $.get('assets/json/softSkillSelected.json', (data:any) => {
      this.chatMessage = data;
      this.chatLength = data.length;

      for (let i = 0; i < this.chatLength-1; i++) {
        this.typingAnimation();
        $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
          let chatData = this.chatMessage[i];


          var appendQuestion =
          `
            <div class="justify-content-start">
              <div class="col-12 pl-0 pr-0">
                <div class="message-wrapper">
                  <div class="message">
                    <div class="text">
                      <p class="mt-2 mb-0 message-text"
                      style="
                      max-width: 100%;
                      border-bottom-right-radius: 20px;
                      border-top-right-radius: 20px;
                      border-top-left-radius: 20px;
                      position: relative;
                      display: inline-block;
                      background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                      width: -webkit-fit-content;
                      width: -moz-fit-content;
                      width: fit-content;
                      padding: 10px;">
                        `+chatData.message+`
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `;
          $(appendQuestion).appendTo(".chat-box-container");

          for (let i = 1; i < this.chatLength; i++) {
            this.typingAnimation();
            $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
              let chatData = this.chatMessage[i];


              var appendQuestion =
            `
              <div class="justify-content-start">
                <div class="col-12 pl-0 pr-0">
                  <div class="message-wrapper">
                    <div class="message">
                      <div class="text">
                        <p class="mt-2 mb-0 message-text"
                        style="
                        max-width: 100%;
                        border-bottom-right-radius: 20px;
                        border-top-right-radius: 20px;
                        border-top-left-radius: 20px;
                        position: relative;
                        display: inline-block;
                        background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                        width: -webkit-fit-content;
                        width: -moz-fit-content;
                        width: fit-content;
                        padding: 10px;">
                          <a href='`+chatData.message+`' target="_blank">`+chatData.message+`<a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            `;
            $(appendQuestion).appendTo(".chat-box-container");
            })
          }
        })
      }
    })

  }

  industrialSkillClick(){
    console.log('Industrial Skill clicked');
    $(".choices").css('display', 'none');

    //append message
    var appendAnswer =
    `
    <div class="row pr5 justify-content-end">
        <div class="col-8 pl-0 pr-0">
            <div class="reply float-right" style="background-color: #fff;
              border-bottom-left-radius: 20px;
              border-top-left-radius: 20px;
              border-top-right-radius: 20px;
              display: inline-block;
              margin-top: .5rem;
              max-width: 100%;
              position: relative;
              margin-right: 15px;">
              <div class="text text-left" style="padding: 10px;">
                I want to ask about Industrial Skill Training
              </div>
            </div>
        </div>
    </div>
    `;
    $(appendAnswer).appendTo(".chat-box-container");

    $.get('assets/json/industrialSkillSelected.json', (data:any) => {
      this.chatMessage = data;
      this.chatLength = data.length;

      for (let i = 0; i < this.chatLength-1; i++) {
        this.typingAnimation();
        $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
          let chatData = this.chatMessage[i];


          var appendQuestion =
          `
            <div class="justify-content-start">
              <div class="col-12 pl-0 pr-0">
                <div class="message-wrapper">
                  <div class="message">
                    <div class="text">
                      <p class="mt-2 mb-0 message-text"
                      style="
                      max-width: 100%;
                      border-bottom-right-radius: 20px;
                      border-top-right-radius: 20px;
                      border-top-left-radius: 20px;
                      position: relative;
                      display: inline-block;
                      background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                      width: -webkit-fit-content;
                      width: -moz-fit-content;
                      width: fit-content;
                      padding: 10px;">
                        `+chatData.message+`
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `;
          $(appendQuestion).appendTo(".chat-box-container");

          for (let i = 1; i < this.chatLength; i++) {
            this.typingAnimation();
            $(".col-message-wrapper").removeClass('hide').delay(1000).fadeIn("normal").delay(1000).fadeOut(() => {
              let chatData = this.chatMessage[i];


              var appendQuestion =
            `
              <div class="justify-content-start">
                <div class="col-12 pl-0 pr-0">
                  <div class="message-wrapper">
                    <div class="message">
                      <div class="text">
                        <p class="mt-2 mb-0 message-text"
                        style="
                        max-width: 100%;
                        border-bottom-right-radius: 20px;
                        border-top-right-radius: 20px;
                        border-top-left-radius: 20px;
                        position: relative;
                        display: inline-block;
                        background-image: linear-gradient(to right, #30c76b, #a5dfcf);
                        width: -webkit-fit-content;
                        width: -moz-fit-content;
                        width: fit-content;
                        padding: 10px;">
                          <a href='`+chatData.message+`' target="_blank">`+chatData.message+`<a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            `;
            $(appendQuestion).appendTo(".chat-box-container");
            })
          }
        })
      }
    })
  }
}
