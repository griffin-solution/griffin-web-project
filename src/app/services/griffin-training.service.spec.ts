import { TestBed } from '@angular/core/testing';

import { GriffinTrainingService } from './griffin-training.service';

describe('GriffinTrainingService', () => {
  let service: GriffinTrainingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GriffinTrainingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
