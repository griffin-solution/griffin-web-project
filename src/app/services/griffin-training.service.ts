import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GriffinTrainingService {

  constructor(private http: HttpClient) { }

  getGriffinTrainingEventCalendar(){
    return this.http.get('assets/data/course-calendar.js').subscribe(data => {
      console.log(data);

    })
  }
}
