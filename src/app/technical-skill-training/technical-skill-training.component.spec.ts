import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalSkillTrainingComponent } from './technical-skill-training.component';

describe('TechnicalSkillTrainingComponent', () => {
  let component: TechnicalSkillTrainingComponent;
  let fixture: ComponentFixture<TechnicalSkillTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnicalSkillTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalSkillTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
